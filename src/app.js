const express = require('express');
const morgan = require('morgan');
const helmet = require('helmet');
const expressOasGenerator = require('express-oas-generator');

require('dotenv').config();

const middlewares = require('./middlewares');
const api = require('./route');

const app = express();

expressOasGenerator.init(app, {});
app.use(morgan('dev'));
app.use(helmet());

app.get('/', (req, res) => {
  res.json({
    message: '🦄🌈✨👋🌎🌍🌏✨🌈🦄'
  });
});

app.use('/api/v1', api);

app.use(middlewares.notFound);
app.use(middlewares.errorHandler);

module.exports = app;
